package answer

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type RangeListTestSuite struct {
	suite.Suite
}

func (s *RangeListTestSuite) SetupTest() {
}

func TestRangeListTestSuit(t *testing.T) {
	suite.Run(t, new(RangeListTestSuite))
}

func (s *RangeListTestSuite) TestSearchFunc() {
	source := [][2]int{{2, 5}, {9, 13}, {100, 200}}
	cases := []struct {
		name     string
		src      [][2]int
		query    [2]int
		wantHead int
		wantTail int
	}{
		{
			name:     "contain_source",
			src:      source,
			query:    [2]int{1, 1000},
			wantHead: -1,
			wantTail: len(source),
		},
		{
			name:     "insert_gap",
			src:      source,
			query:    [2]int{6, 8},
			wantHead: 0,
			wantTail: 1,
		},
		{
			name:     "contained_by_source",
			src:      source,
			query:    [2]int{9, 10},
			wantHead: 1,
			wantTail: 1,
		},
		{
			name:     "grow_right",
			src:      source,
			query:    [2]int{4, 7},
			wantHead: 0,
			wantTail: 1,
		},
		{
			name:     "grow_left",
			src:      source,
			query:    [2]int{6, 10},
			wantHead: 0,
			wantTail: 1,
		},
		{
			name:     "grow_left_2",
			src:      source,
			query:    [2]int{1, 10},
			wantHead: -1,
			wantTail: 1,
		},
		{
			name:     "grow_right",
			src:      source,
			query:    [2]int{9, 20},
			wantHead: 1,
			wantTail: 2,
		},
		{
			name:     "grow_right_2",
			src:      source,
			query:    [2]int{100, 300},
			wantHead: 2,
			wantTail: 3,
		},
		{
			name:     "link",
			src:      source,
			query:    [2]int{3, 200},
			wantHead: 0,
			wantTail: 2,
		},
	}

	for _, tt := range cases {
		head, tail := search(tt.src, tt.query)
		s.Equal(tt.wantHead, head)
		s.Equal(tt.wantTail, tail)
	}
}

func (s *RangeListTestSuite) TestAdd() {
	cases := []struct {
		name    string
		ele     [2]int
		source  [][2]int
		wantErr error
		want    [][2]int
	}{
		{
			name:    "invalid_ele",
			wantErr: EndCannotLessThenStart,
			ele:     [2]int{5, 1},
		},
		{
			name: "list_empty",
			ele:  [2]int{1, 5},
			want: [][2]int{{1, 5}},
		},
		{
			name:   "list_one_ele",
			source: [][2]int{{2, 4}},
			ele:    [2]int{2, 3},
			want:   [][2]int{{2, 4}},
		},
		{
			name:   "list_one_ele_grow_left_right",
			source: [][2]int{{2, 4}},
			ele:    [2]int{1, 5},
			want:   [][2]int{{1, 5}},
		},
		{
			name:   "list_one_ele_grow_left",
			source: [][2]int{{2, 4}},
			ele:    [2]int{1, 3},
			want:   [][2]int{{1, 4}},
		},
		{
			name:   "list_one_ele_grow_right",
			source: [][2]int{{2, 4}},
			ele:    [2]int{4, 6},
			want:   [][2]int{{2, 6}},
		},
		{
			name:   "list_one_ele_insert_right",
			source: [][2]int{{2, 4}},
			ele:    [2]int{5, 6},
			want:   [][2]int{{2, 4}, {5, 6}},
		},
		{
			name:   "list_one_ele_insert_left",
			source: [][2]int{{2, 4}},
			ele:    [2]int{-1, 1},
			want:   [][2]int{{-1, 1}, {2, 4}},
		},
	}

	for _, tt := range cases {
		s.Run(tt.name, func() {
			rl := &RangeList{container: tt.source}
			err := rl.Add(tt.ele)
			s.Equal(tt.wantErr, err)
			s.Equal(tt.want, rl.container)
		})
	}
}

func (s *RangeListTestSuite) TestAdd2() {
	src := [][2]int{{3, 5}, {9, 14}, {100, 200}}
	cases := []struct {
		name    string
		ele     [2]int
		source  [][2]int
		wantErr error
		want    [][2]int
	}{
		{
			name:   "add_zero",
			source: src,
			ele:    [2]int{6, 6},
			want:   [][2]int{{3, 5}, {9, 14}, {100, 200}},
		},
		{
			name:   "list_insert_mid",
			source: src,
			ele:    [2]int{6, 7},
			want:   [][2]int{{3, 5}, {6, 7}, {9, 14}, {100, 200}},
		},
		{
			name:   "list_insert_left",
			source: src,
			ele:    [2]int{1, 2},
			want:   [][2]int{{1, 2}, {3, 5}, {9, 14}, {100, 200}},
		},
		{
			name:   "list_insert_right",
			source: src,
			ele:    [2]int{220, 300},
			want:   [][2]int{{3, 5}, {9, 14}, {100, 200}, {220, 300}},
		},
		{
			name:   "list_link_1",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{4, 10},
			want:   [][2]int{{3, 14}, {100, 200}},
		},
		{
			name:   "list_link_2",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{4, 110},
			want:   [][2]int{{3, 200}},
		},
		{
			name:   "list_link_3",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{1, 300},
			want:   [][2]int{{1, 300}},
		},
		{
			name:   "list_link_4",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{1, 110},
			want:   [][2]int{{1, 200}},
		},
		{
			name:   "list_link_5",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{9, 300},
			want:   [][2]int{{3, 5}, {9, 300}},
		},
		{
			name:   "list_grow_1",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{9, 20},
			want:   [][2]int{{3, 5}, {9, 20}, {100, 200}},
		},
		{
			name:   "list_grow_2",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{90, 200},
			want:   [][2]int{{3, 5}, {9, 14}, {90, 200}},
		},
		{
			name:   "list_skip",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{9, 10},
			want:   [][2]int{{3, 5}, {9, 14}, {100, 200}},
		},
	}

	for _, tt := range cases {
		s.Run(tt.name, func() {
			s.T().Logf("src=%v ", tt.source)
			rl := &RangeList{container: tt.source}
			err := rl.Add(tt.ele)
			s.Equal(tt.wantErr, err)
			s.Equal(tt.want, rl.container)
		})
	}
}

func (s *RangeListTestSuite) TestRemove() {
	// src := [][2]int{{3, 5}, {9, 14}, {100, 200}}
	cases := []struct {
		name    string
		ele     [2]int
		source  [][2]int
		wantErr error
		want    [][2]int
	}{
		{
			name:    "invalid_ele",
			wantErr: EndCannotLessThenStart,
			ele:     [2]int{5, 1},
		},
		{
			name:   "list_empty",
			source: nil,
			ele:    [2]int{6, 7},
			want:   nil,
		},
		{
			name:   "list_remove_all",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{1, 300},
			want:   [][2]int{},
		},
		{
			name:   "list_remove_left_all",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{1, 110},
			want:   [][2]int{{110, 200}},
		},
		{
			name:   "list_remove_left_all_1",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{1, 8},
			want:   [][2]int{{9, 14}, {100, 200}},
		},
		{
			name:   "list_remove_right_all",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{9, 300},
			want:   [][2]int{{3, 5}, {9, 9}},
		},
		{
			name:   "list_remove_right_all_2",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{8, 300},
			want:   [][2]int{{3, 5}},
		},
	}

	for _, tt := range cases {
		s.Run(tt.name, func() {
			rl := &RangeList{container: tt.source}
			err := rl.Remove(tt.ele)
			s.Equal(tt.wantErr, err)
			s.Equal(tt.want, rl.container)
		})
	}
}

func (s *RangeListTestSuite) TestRemove2() {
	cases := []struct {
		name    string
		ele     [2]int
		source  [][2]int
		wantErr error
		want    [][2]int
	}{
		{
			name:   "list_remove_zero_1",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{4, 4},
			want:   [][2]int{{3, 5}, {9, 14}, {100, 200}},
		},
		{
			name:   "list_remove_zero_2",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{8, 8},
			want:   [][2]int{{3, 5}, {9, 14}, {100, 200}},
		},
		{
			name:   "list_remove_miss",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{6, 8},
			want:   [][2]int{{3, 5}, {9, 14}, {100, 200}},
		},
		{
			name:   "list_remove_modified_right",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{6, 110},
			want:   [][2]int{{3, 5}, {110, 200}},
		},
		{
			name:   "list_remove_modified_left",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{4, 8},
			want:   [][2]int{{3, 4}, {9, 14}, {100, 200}},
		},
		{
			name:   "list_remove_modified",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{4, 110},
			want:   [][2]int{{3, 4}, {110, 200}},
		},
		{
			name:   "list_remove_modified_1",
			source: [][2]int{{3, 5}, {9, 14}, {100, 200}},
			ele:    [2]int{101, 110},
			want:   [][2]int{{3, 5}, {9, 14}, {100, 101}, {110, 200}},
		},
	}

	for _, tt := range cases {
		s.Run(tt.name, func() {
			rl := &RangeList{container: tt.source}
			err := rl.Remove(tt.ele)
			s.Equal(tt.wantErr, err)
			s.Equal(tt.want, rl.container)
		})
	}
}
