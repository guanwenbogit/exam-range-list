package answer

import "fmt"

// Index flag
const (
	Left  = 0
	Right = 1
)

// NewRangeList new RangeList struct
func NewRangeList() *RangeList {
	return &RangeList{container: [][2]int{}}
}

// RangeList a list contains rangeElement
type RangeList struct {
	container [][2]int
}

// Add add rangeElement [2]int to list
func (r *RangeList) Add(rangeElement [2]int) error {
	if err := rangeElementInvalid(rangeElement); err != nil {
		return err
	}

	if rangeElement[0] == rangeElement[1] {
		return nil
	}

	if len(r.container) == 0 {
		r.container = append(r.container, rangeElement)

		return nil
	}

	length := len(r.container)
	head, tail := search(r.container, rangeElement)

	if head == -1 && tail == length {
		r.container = [][2]int{rangeElement}

		return nil
	}

	if head == -1 {
		right := r.container[tail]
		if rangeElement[Right] < right[Left] {
			r.container = append([][2]int{rangeElement}, r.container[tail:]...)
			return nil
		}

		r.container[tail][Left] = rangeElement[Left]
		r.container = r.container[tail:]

		return nil
	}

	if tail == length {
		left := r.container[head]

		if left[Right] < rangeElement[Left] {
			r.container = append(r.container[0:head+1], rangeElement)
			return nil
		}

		r.container[head][Right] = rangeElement[Right]
		r.container = r.container[0 : head+1]

		return nil
	}

	left := r.container[head]
	right := r.container[tail]
	if left[Right] < rangeElement[Left] && rangeElement[Right] < right[Left] {
		tmp := append([][2]int{}, r.container[0:head+1]...)
		tmp = append(tmp, rangeElement)
		tmp = append(tmp, r.container[tail:]...)
		r.container = tmp

		return nil
	}

	if left[Right] < rangeElement[Left] && right[Left] <= rangeElement[Right] {
		r.container[tail][Left] = rangeElement[Left]
		tmp := append([][2]int{}, r.container[0:head+1]...)
		tmp = append(tmp, r.container[tail:]...)
		r.container = tmp

		return nil
	}

	if left[Right] >= rangeElement[Left] && rangeElement[Right] < right[Left] {
		r.container[head][Right] = rangeElement[Right]
		tmp := append([][2]int{}, r.container[0:head+1]...)
		tmp = append(tmp, r.container[tail:]...)
		r.container = tmp

		return nil
	}

	if left[Right] >= rangeElement[Left] && rangeElement[Right] >= right[Left] {
		r.container[tail][Left] = left[Left]
		tmp := append([][2]int{}, r.container[0:head]...)
		tmp = append(tmp, r.container[tail:]...)
		r.container = tmp

		return nil
	}

	return nil
}

func search(src [][2]int, rangeElement [2]int) (int, int) {
	left, right := rangeElement[Left], rangeElement[Right]

	var i, head, tail int
	l := len(src)

	for i = l - 1; i >= 0; i-- {
		cur := src[i]
		if cur[Left] <= left {
			head = i
			break
		}

		head = -1
	}

	if i < 0 {
		i = 0
	}

	for ; i < l; i++ {
		cur := src[i]
		if cur[Right] >= right {
			tail = i
			break
		}

		tail = l
	}

	return head, tail
}

// Remove remove rangeElement [2]int from list
func (r *RangeList) Remove(rangeElement [2]int) error {
	if err := rangeElementInvalid(rangeElement); err != nil {
		return err
	}

	if rangeElement[0] == rangeElement[1] {
		return nil
	}

	if len(r.container) == 0 {
		return nil
	}

	length := len(r.container)
	head, tail := search(r.container, rangeElement)
	if head == -1 && tail == length {
		r.container = make([][2]int, 0)
		return nil
	}

	if head == -1 {
		right := r.container[tail]
		if rangeElement[Right] < right[Left] {
			r.container = r.container[tail:]
			return nil
		}

		r.container[tail][Left] = rangeElement[Right]
		r.container = r.container[tail:]

		return nil
	}

	if tail == length {
		left := r.container[head]

		if left[Right] < rangeElement[Left] {
			r.container = r.container[0 : head+1]

			return nil
		}

		r.container[head][Right] = rangeElement[Left]
		r.container = r.container[0 : head+1]

		return nil
	}

	if head == tail {
		cur := r.container[head]
		tmp := append([][2]int{}, r.container[0:head]...)
		if cur[Left] < rangeElement[Left] {
			tmp = append(tmp, [2]int{cur[Left], rangeElement[Left]})
		}
		if rangeElement[Right] < cur[Right] {
			tmp = append(tmp, [2]int{rangeElement[Right], cur[Right]})
		}

		tmp = append(tmp, r.container[head+1:]...)
		r.container = tmp

		return nil
	}

	left := r.container[head]
	right := r.container[tail]
	if left[Right] < rangeElement[Left] && rangeElement[Right] < right[Left] {
		return nil
	}

	if left[Right] < rangeElement[Left] && right[Left] <= rangeElement[Right] {
		r.container[tail][Left] = rangeElement[Right]
		tmp := append([][2]int{}, r.container[0:head+1]...)
		tmp = append(tmp, r.container[tail:]...)
		r.container = tmp

		return nil
	}

	if left[Right] >= rangeElement[Left] && rangeElement[Right] < right[Left] {
		r.container[head][Right] = rangeElement[Left]
		tmp := append([][2]int{}, r.container[0:head+1]...)
		tmp = append(tmp, r.container[tail:]...)
		r.container = tmp

		return nil
	}

	if left[Right] >= rangeElement[Left] && rangeElement[Right] >= right[Left] {
		r.container[head][Right] = rangeElement[Left]
		r.container[tail][Left] = rangeElement[Right]
		tmp := append([][2]int{}, r.container[0:head+1]...)
		tmp = append(tmp, r.container[tail:]...)
		r.container = tmp

		return nil
	}

	return nil
}

// Print print into stdout/stderr
func (r *RangeList) Print() {
	for _, ele := range r.container {
		print(ele)
	}

	fmt.Println("")
}

// print func
func print(rangeElement [2]int) {
	fmt.Printf("[%d, %d)", rangeElement[0], rangeElement[1])
}

// rangeElementInvalid verify range element
func rangeElementInvalid(rangeElement [2]int) error {
	if rangeElement[0] > rangeElement[1] {
		return EndCannotLessThenStart
	}

	return nil
}
