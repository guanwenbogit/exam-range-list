package answer

// RangeListErr range list err
type RangeListErr string

// Error imp error interface
func (e RangeListErr) Error() string {
	return string(e)
}

// Error constants
const (
	EndCannotLessThenStart = RangeListErr("end value can not less then start")
)
